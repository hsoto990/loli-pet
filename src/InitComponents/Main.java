package InitComponents;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Main extends JFrame {
	private static final long serialVersionUID = 1L;
	public static JPanel contentPane;

	public static String lugaPlace = "Index";
	public static String Pose = "De pie";
	public static String inMano = "Index";
	public static boolean AutoMode = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	JLabel label = new JLabel("");
	JLabel label1_2 = new JLabel("");
	JLabel labelFace = new JLabel("");
	JPanel World = new JPanel();

	// static Point playerLocation= new Point (200,300);
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(244, 0, 640, 480);
		contentPane = new JPanel();
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				
				if (arg0.getKeyChar() == 'a')
					
					World.setLocation(World.getX() + 32, World.getY());
				if (arg0.getKeyChar() == 'w')
					World.setLocation(World.getX(), World.getY() + 32);
				if (arg0.getKeyChar() == 'd')
					World.setLocation(World.getX() - 32, World.getY());
				if (arg0.getKeyChar() == 's')
					World.setLocation(World.getX(), World.getY() - 32);
				
				labelFace.setLocation((640 - 32) / 2 - World.getLocation().x, (480 - 32) / 2 - World.getLocation().y);

			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		setUndecorated(true);

		World.setBounds(-90* 32, -90* 32, 271 * 32, 261 * 32);
		World.setLayout(null);
		contentPane.add(World);

		labelFace.setIcon(new ImageIcon("/home/ceibal/Escritorio/Resources/Face.png"));
		labelFace.setBounds(90* 32 +(640 - 32) / 2, 90* 32+ (480 - 32) / 2, 32, 32);
		repaint();
		World.add(labelFace);
		System.out.println(new Color(152,122,0).getRGB());
		saaaaBlock();
		
	}

	void saaaaBlock() {
		new Thread() {
			public void run() {
				int aaaaa=0;
				try {
					BufferedImage icomand = ImageIO.read(new File("/home/ceibal/Escritorio/Mapasa/map.png"));
					for (int iy = 0; iy < icomand.getHeight(); iy++) {
						for (int ix = 0; ix < icomand.getWidth(); ix++) {
							JLabel label = new JLabel("");
							label.setIcon(new ImageIcon(
									"/home/ceibal/Escritorio/Mapasa/text/" + icomand.getRGB(ix, iy) + ".png"));
							label.setBounds(ix * 32, iy * 32, 32, 32);
							World.add(label);
						}
						repaint();
					}
				} catch (Exception e) {
					System.out.println(aaaaa);
				}
			}
		}.start();
	}

}
