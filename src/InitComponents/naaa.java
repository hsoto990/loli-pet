package InitComponents;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class naaa extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					naaa frame = new naaa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	static JLabel lblNewLabel = new JLabel("");
	static JLabel lblLis = new JLabel("");
	static JPanel lblNewPanel = new JPanel();
	static double lisCoorX = 60;
	static double lisCoorY = 1060;
	ArrayList<Room> sa = new ArrayList<Room>();
	ArrayList<Connection> samo = new ArrayList<Connection>();

	public naaa() {
		for (int i = 0; i < 12; i++)
			sa.add(new Room());
		sa.get(1).configure("Cocina", 31, 8, 64, 82);
		sa.get(2).configure("CocinaCorner", 65, 79, 84, 82);
		sa.get(7).configure("postPreCocina", 31, 83, 52, 96);
		sa.get(3).configure("PreCocina", 10, 97, 52, 205);
		sa.get(4).configure("PrePreCocina", 53, 160, 102, 205);
		sa.get(5).configure("PrePrePreCocina", 103, 100, 196, 205);
		sa.get(6).configure("PreComedor", 184, 206, 196, 228);
		sa.get(8).configure("Comedor", 184, 229, 214, 280);
		sa.get(9).configure("Comedor2", 215, 223, 262, 280);
		sa.get(10).configure("Comedor3", 263, 223, 306, 265);
		sa.get(11).configure("Comedor4", 184, 281, 208, 332);
		
		for (int i = 1; i < sa.size() - 1; i++)
			for (int i2 = i + 1; i2 < sa.size(); i2++)
				waltToNodeaass(i, i2);

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new Thread() {
					public void run() {
						System.out.println(getroom(arg0.getX() - lblNewPanel.getX(), arg0.getY() - lblNewPanel.getY())
								+ "-" + getroom((int) lisCoorX, (int) lisCoorY));
						if (getroom((int) lisCoorX, (int) lisCoorY) == getroom(arg0.getX() - lblNewPanel.getX(),
								arg0.getY() - lblNewPanel.getY()) | getroom((int) lisCoorX, (int) lisCoorY) == 0)
							moves(arg0.getX() - lblNewPanel.getX(), arg0.getY() - lblNewPanel.getY());
						else {
							int mx = arg0.getX();
							int my = arg0.getY();

							for (int i = 0; i < samo.size(); i++) {
								Connection thisamo = samo.get(i);
								waltToNode(mx, my, thisamo.room1, thisamo.room2, thisamo.xnode, thisamo.ynode,
										thisamo.xmod, thisamo.ymod);
							}
						}
					}
				}.start();
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyChar() == 'w')
					lblNewPanel.setLocation(lblNewPanel.getX(), lblNewPanel.getY() + 5);
				if (arg0.getKeyChar() == 's')
					lblNewPanel.setLocation(lblNewPanel.getX(), lblNewPanel.getY() - 5);
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 372, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblNewPanel.setLayout(null);
		setUndecorated(true);
		lblNewPanel.setBounds(0, 0, 372, 1095);

		lblLis.setIcon(new ImageIcon("/home/ceibal/Escritorio/Resources/Face2.png"));
		lblLis.setBounds((int) lisCoorX, (int) lisCoorY, 16, 16);
		lblNewPanel.add(lblLis);

		lblNewLabel.setIcon(new ImageIcon("/home/ceibal/Escritorio/Caaaasaaaaaa.png"));
		lblNewLabel.setBounds(0, 0, 372, 1095);

		contentPane.add(lblNewPanel);
		lblNewPanel.add(lblNewLabel);
	}

	int actualmove = 0;

	void moves(int desX, int desY) {
		actualmove++;
		int moovithis = actualmove;
		int deltaX = desX - (int) lisCoorX;
		int deltaY = desY - (int) lisCoorY;

		int dis = (int) Math.sqrt(deltaX * deltaX + deltaY * deltaY) / 10;

		for (int i = 0; i < dis * 2; i++) {
			if (moovithis == actualmove) {
				lisCoorX += ((deltaX) / (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / 10)) / 2;
				lisCoorY += ((deltaY) / (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / 10)) / 2;
				lblLis.setBounds((int) lisCoorX - 16 + 8, (int) lisCoorY - 22 + 5, 32, 32);

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			} else
				break;

		}
		lisCoorX = desX;
		lisCoorY = desY;
		lblLis.setBounds((int) lisCoorX - 16 + 8, (int) lisCoorY - 22 + 5, 32, 32);

	}

	void waltToNodeaass(int room1, int room2) {
		Room rum1 = sa.get(room1);
		Room rum2 = sa.get(room2);
		int xnode = 0;
		int ynode = 0;
		int xmod = 0;
		int ymod = 0;
		int firsX = 0;
		int lastX = 0;
		int firsY = 0;
		int lastY = 0;

		if (rum1.x1 == rum2.x2 + 1) {
			xnode = rum2.x2;
			xmod = -1;
		} else if (rum1.x2 + 1 == rum2.x1) {
			xnode = rum1.x2;
			xmod = 1;
		} else {

			if (rum1.x1 >= rum2.x1)
				firsX = rum1.x1;
			else
				firsX = rum2.x1;
			if (rum1.x2 <= rum2.x2)
				lastX = rum1.x2;
			else
				lastX = rum2.x2;
			xnode = (firsX + lastX) / 2;
		}

		if (rum1.y1 == rum2.y2 + 1) {
			ynode = rum2.y2;
			ymod = -1;
		} else if (rum1.y2 + 1 == rum2.y1) {
			ynode = rum1.y2;
			ymod = 1;
		} else {
			if (rum1.y1 >= rum2.y1)
				firsY = rum1.y1;
			else
				firsY = rum2.y1;
			if (rum1.y2 <= rum2.y2)
				lastY = rum1.y2;
			else
				lastY = rum2.y2;
			ynode = (firsY + lastY) / 2;
		}

		if (xmod == 0 ^ ymod == 0) {
			if (rum1.y1 <= rum2.y2 + 1 & rum1.x1 <= rum2.x2 + 1) {
				if (rum1.y2 + 1 >= rum2.y1 & rum1.x2 + 1 >= rum2.x1) {

					// if ((firsX != 0 & lastX != 0 & firsY != 0 & lastY != 0)) {
					// System.out.println("aaaaaaaa " + room1 + " " + room2 + " " + xnode + "-" + ynode +" "+ymod);
					// System.out.println(firsX + " " + lastX + " " + firsY + " " + lastY);

					samo.add(new Connection());
					Grafosia.addNode(xnode, ynode);

					samo.get(samo.size() - 1).configure(room1, room2, xnode, ynode, xmod, ymod);
					Grafosia.addDobleArco(room1, room2);
					
				}
			}
		}
	}

	void waltToNode(int mx, int my, int room1, int room2, int xnode, int ynode, int xmod, int ymod) {
		if ((getroom(mx - lblNewPanel.getX(), my - lblNewPanel.getY())) == room1) {
			if (getroom((int) lisCoorX, (int) lisCoorY) == room2)
				moves(xnode, ynode - ymod);
		} else if ((getroom(mx - lblNewPanel.getX(), my - lblNewPanel.getY())) == room2)
			if (getroom((int) lisCoorX, (int) lisCoorY) == room1)
				moves(xnode + xmod, ynode + ymod);
	}

	int getroom(int x, int y) {
		int i = 0;
		int saturn = 0;
		for (i = 0; i < sa.size(); i++)
			if (sa.get(i).isHere(x, y)) {
				saturn = i;
				break;
			}
		return saturn;
	}

	static boolean valueInCords(int mx, int my, int x, int y, int x2, int y2) {
		return (mx >= x & mx <= x2) & (my >= y & my <= y2);
	}
}
