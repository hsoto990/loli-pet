package InitComponents;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

public class Grafosia {
	// Arcos
	static ArrayList<ArrayList<Integer>> Destino = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Double>> Peso = new ArrayList<ArrayList<Double>>();

	// Nodos
	static ArrayList<Point> Nodo = new ArrayList<Point>();

	static void addNode(int x, int y) {
		Destino.add(new ArrayList<Integer>());
		Peso.add(new ArrayList<Double>());
		Nodo.add(new Point(x, y));
	}

	static void addArco(int n1, int n2) {
		addFinesArco(n1, n2, getNodeDistance(n1, n2));
	}

	static void addDobleArco(int n1, int n2) {
		addArco(n1, n2);
		addArco(n2, n1);
	}

	static void addFinesArco(int n1, int n2, double pg) {
		Destino.get(n1).add(n2);
		Peso.get(n1).add(pg);
	}

	static double getNodeDistance(int n1, int n2) {
		return Math.hypot((Nodo.get(n1).x - Nodo.get(n2).x), (Nodo.get(n1).y - Nodo.get(n2).y));
	}

	static int end = 0;

	static ArrayList<Integer> Resolve(Graphics2D g, int vstart, int vend, double max) {
		end = vend;
		RutasEncontradas.removeAll(RutasEncontradas);
		DistanciasAquí.removeAll(DistanciasAquí);
		if (vstart == end) {
			ArrayList<Integer> saaad =new ArrayList<Integer>();
			saaad.add(end);
			return saaad;
		} else {
			return looplikesearch(vstart, max, new ArrayList<Integer>());
		}

	}

	static double maxstamina = 0;

	static ArrayList<Integer> looplikesearch(int actual, double stamina, ArrayList<Integer> visitados) {
		maxstamina = stamina;
		ArrayList<Integer> actualant = new ArrayList<Integer>();
		actualant.add(actual);
		supa(actualant, 0);
		if (RutasEncontradas.size() != 0) {
			int elegido = 0;
			int bestDis = DistanciasAquí.get(0);
			for (int i = 1; i < RutasEncontradas.size(); i++) {
				if (DistanciasAquí.get(i) < bestDis) {
					bestDis = DistanciasAquí.get(i);
					elegido = i;
				}
			}
			System.out.println("Mejor Ruta: " + RutasEncontradas.get(elegido) + "\nDistancia=" + bestDis);
			return RutasEncontradas.get(elegido);
		} else {
			System.out.println("No hay rutas encontradas");
			return null;
		}
	}

	static ArrayList<ArrayList<Integer>> RutasEncontradas = new ArrayList<ArrayList<Integer>>();
	static ArrayList<Integer> DistanciasAquí = new ArrayList<Integer>();

	static void supa(ArrayList<Integer> actualant, double stamina) {
		for (int i = 0; i < Destino.get(actualant.get(actualant.size() - 1)).size(); i++) {
			if (!actualant.contains(Destino.get(actualant.get(actualant.size() - 1)).get(i))) {
				stamina += Peso.get(actualant.get(actualant.size() - 1)).get(i);
				if (stamina <= maxstamina) {
					actualant.add(Destino.get(actualant.get(actualant.size() - 1)).get(i));
					if (Destino.get(actualant.get(actualant.size() - 2)).get(i) != end) {
						supa(actualant, stamina);
					} else {
						RutasEncontradas.add(new ArrayList<Integer>());
						for (int k = 0; k < actualant.size(); k++)
							RutasEncontradas.get(RutasEncontradas.size() - 1).add(actualant.get(k));

						DistanciasAquí.add((int) (stamina));
					}
					actualant.remove(actualant.size() - 1);
				}
				stamina -= Peso.get(actualant.get(actualant.size() - 1)).get(i);
			}
		}
	}

	// static void looplikesearch(int vactual, double vstamina, ArrayList<Integer>
	// Vvisitados) {
	// int actual= vactual;
	// double stamina= vstamina;
	// ArrayList<Integer> visitados = Vvisitados;
	// visitados.add(actual);
	//
	// if (actual == end)
	// System.out.println("Encontrado! Estamina=" + (int) stamina + " Ruta=" +
	// visitados);
	// else
	// // new Thread() {
	// // public void run() {
	// System.out.println(visitados);
	// for (int i = 0; i < Destino.get(actual).size(); i++) {
	// int miju = Destino.get(actual).get(i);
	// if (!visitados.contains(miju)) {
	// double newStamina = stamina - Peso.get(actual).get(i);
	// if (newStamina >= 0) {
	// looplikesearch(Destino.get(actual).get(i), newStamina, visitados);
	// } else
	// System.out.println("Failure");
	// }
	//
	// }
	// // }
	// // }.start();
	// }
}
