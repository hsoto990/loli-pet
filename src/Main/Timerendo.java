package Main;

public class Timerendo {

	public static int veltime = 100;
	
	public static long dia = 1;
	public static int hora = 7;
	public static int minu = 0;

	public static int hungerBar = 0;
	public static int sleepBar = 24*60;
	public static int cleanBar = 32*60;
	public static int funBar = 64*60;
	public static int hungerBarMax = 1000;
	public static int sleepBarMax = 24*60;
	public static int cleanBarMax = 32*60;
	public static int funBarMax = 64*60;
	
	
	public static double IMCValue = 24;

	
	public static void start() {

		new Thread() {
			public void run() {
				while (true) {
					
					for (byte b = 0; b < 30; b++) {
						try {
							Thread.sleep(veltime);
						} catch (InterruptedException e) {
						}
						minu++;
						if (minu == 60) {
							minu = 0;
							hora++;
							if (hora == 24) {
								hora = 0;
								dia++;
							} else if (Main.AutoMode) {
								if (hora == 22) {
									new Thread() {
										public void run() {
											AutoMoves.sleep();
										}
									}.start();
								} else if (hora == 6) {
									new Thread() {
										public void run() {
											AutoMoves.awake();
										}
									}.start();
								} else if (hora == 7 | hora == 12 | hora == 18) {
									new Thread() {
										public void run() {
											AutoMoves.eat(7);
										}
									}.start();
								} else if (hora == 20) {
									new Thread() {
										public void run() {
											AutoMoves.shower();
										}
									}.start();
								}
							}

						}
						
						if (!Main.Pose.equals("Dormida")) {
							hungerBar -=1;
							if (hungerBar <= 0)
								IMCValue -= 0.0005;
							else
								IMCValue += 0.0005;
						}
					}

					if (Main.Pose.equals("Dormida")) {
						sleepBar +=90;
						cleanBar -=5;
						hungerBar -=5;
						if (hungerBar <= 0)
							IMCValue -= 0.0025;
						else
							IMCValue += 0.0025;
					} else {
						sleepBar -=30;
						cleanBar -=30;
						funBar -=40;
					}
				}
			}
		}.start();
	}
	
}
