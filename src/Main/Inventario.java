package Main;

import javax.swing.JOptionPane;

public class Inventario {
	static int money = 1000;
	
	static String[] name = {"Azucar","Harina","Arroz","Pasta","Leche","Huevos","Ración","Medicina","Fruta","Salsa","Verdura","Carne"};
	static double[] quantity = {0,0,0,0,0,0,0,0,0,0,0,0};
	static double[] price = {50,50,50,60,40,40,50,5000,75,75,75,100};

	public static void Comprar(int indice) {
		if (money >= price[indice]) {
			JOptionPane.showMessageDialog(Main.contentPane, "Has comprado "+name[indice], "Compra realizada", 1);
			quantity[indice]++;
			money -= price[indice];
		}else
			JOptionPane.showMessageDialog(Main.contentPane, "Dinero insuficiente", "Compra infructuosa", 0);
	}
	
}
