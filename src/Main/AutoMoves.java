package Main;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;

public class AutoMoves {
	static boolean desocupated = true;
	static Robot rob;

	public static void eat(int imax) {

		if (desocupated) {
			for (int i = 0; i < imax; i++) {
				desocupated = false;
				if (!Main.lugaPlace.equals("Kitchen")) {
					mouseMoveTo(150, 564);
					esperar(50);
					mouseClick();
					esperar(50);
				}
				if (Main.Pose != "Sentada") {
					mouseMoveTo(400, 80);
					esperar(50);
					mouseClick();
					esperar(50);
				}
				if (Main.inMano == "Chopstick") {
					mouseMoveTo(165, 315);
					esperar(50);
					mouseClick();
					esperar(50);
				}
				if (Main.Pose == "Sentada")
					mouseMoveTo(180, 212);
				else
					mouseMoveTo(180, 119);
				esperar(50);
				mouseClick();
				esperar(100);
			}
			mouseMoveTo(75, 564);
			esperar(100);
			mouseClick();
			esperar(100);
			desocupated = true;
		}
	}

	public static void shower() {
		if (desocupated) {
			desocupated = false;
			if (!Main.lugaPlace.equals("bathroom")) {
				mouseMoveTo(190, 564);
				esperar(100);
				mouseClick();
				esperar(100);
			}
			if (!Main.inMano.equals("Esponja")) {
				mouseMoveTo(245, 210);
				esperar(100);
				mouseClick();
				esperar(100);
			}
			mouseMoveTo(150, 160);
			mousePress();
			for (int i = 0; i < 7; i++) {
				mouseMoveToWithQuantity(150 + 10 * i, 500, 100);
				mouseMoveToWithQuantity(150 + 10 * i, 160, 100);
			}
			mouseRelease();

			mouseMoveTo(245, 210);
			esperar(20);
			mouseClick();
			esperar(20);

			mouseMoveToWithQuantity(175, 120, 100);
			esperar(10);
			mouseClick();
			desocupated = true;
		}
	}

	public static void sleep() {
		if (desocupated) {
			desocupated = false;
			if (!Main.lugaPlace.equals("bedroom")) {
				mouseMoveTo(270, 564);
				esperar(100);
				mouseClick();
				esperar(100);
			}
			if (Main.Pose != "Dormida") {
				mouseMoveTo(300, 300);
				esperar(100);
				mouseClick();
				esperar(100);
			}
			desocupated = true;
		}
	}

	public static void awake() {
		if (desocupated) {
			desocupated = false;
			if (Main.Pose == "Dormida") {
				mouseMoveTo(300, 300);
				esperar(100);
				mouseClick();
				esperar(100);
			}
			mouseMoveTo(75, 564);
			esperar(100);
			mouseClick();
			esperar(100);
			desocupated = true;
		}
	}

	static void mouseMoveTo(int XDestin, int YDestin) {
		initRobot();
		XDestin += 344;
		Point actMousePot = MouseInfo.getPointerInfo().getLocation();
		while (actMousePot.getX() != XDestin | actMousePot.getY() != YDestin) {
			actMousePot = MouseInfo.getPointerInfo().getLocation();
			rob.mouseMove((int) (actMousePot.getX() - ((actMousePot.getX() - XDestin) / 10)),
					(int) (actMousePot.getY() - (actMousePot.getY() - YDestin) / 10));
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			if (actMousePot.getX() - XDestin < 10 & actMousePot.getX() - XDestin > -10
					& actMousePot.getY() - YDestin < 10 & actMousePot.getY() - YDestin > -10)
				rob.mouseMove(XDestin, YDestin);
		}
	}

	static void mouseMoveToWithQuantity(int XDestin, int YDestin, int quantity) {
		initRobot();
		XDestin += 344;
		Point actMousePot = MouseInfo.getPointerInfo().getLocation();
		double sgwiX = ((actMousePot.getX() - XDestin) / quantity);
		double sgwiY = ((actMousePot.getY() - YDestin) / quantity);
		for (int i = 0; i < quantity; i++) {
			rob.mouseMove((int) (actMousePot.getX() - sgwiX * i), (int) (actMousePot.getY() - sgwiY * i));
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
			}
		}
	}

	static void esperar(int tiempo) {
		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
		}
	}

	static void mousePress() {
		initRobot();
		rob.mousePress(InputEvent.BUTTON1_DOWN_MASK);
	}

	static void mouseRelease() {
		initRobot();
		rob.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}

	static void mouseClick() {
		mousePress();
		mouseRelease();
	}
	
	static void initRobot() {
		try {
			rob = new Robot();
		} catch (AWTException e) {
		}
	}
}
