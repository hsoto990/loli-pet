package Main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main extends JFrame {
	private static final long serialVersionUID = 1L;
	public static JPanel contentPane;

	public static String lugaPlace = "Index";
	public static String Pose = "De pie";
	public static String inMano = "Index";
	public static boolean AutoMode = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
					frame.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(344, 0, 437, 550);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				EventfromRooms.saaa(arg0.getX(), arg0.getY());
			}
		});

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		setUndecorated(true);
		Main.changeCursor("Pet/Mano", 12, 12);

		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (inMano == "Esponja" & dispon == true) {
					Timerendo.cleanBar += 15;
					Espuma.add(new Point(arg0.getX() - 22, arg0.getY() - 19));
					dispon = false;
					new Thread() {
						public void run() {
							AutoMoves.esperar(10);
							Main.dispon = true;
						}
					}.start();
				}
			}
		});
		contentPane.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (!(lugaPlace.equals("Index") | lugaPlace.equals("Shop")) & inMano == "Mano") {
					try {
						BufferedImage icomand = ImageIO
								.read(new File("/home/ceibal/Escritorio/Resources/Poses/hotscale.png"));
							int toucheda = new Color(icomand.getRGB(arg0.getX(), arg0.getY() -PoseYjthMod)).getBlue();
							if (Pose == "Sentada")
								toucheda = new Color(icomand.getRGB(arg0.getX(), arg0.getY() - 92 -PoseYjthMod))
										.getBlue();

							if (toucheda == 0) {
							
							}else if (toucheda > intimityProgress + 10)
								petGraphicReaction(2, 2, 6, -0.05, 4);
							else if (toucheda > intimityProgress - 30)
								petGraphicReaction(1, 1, 5, -0.1, 1);
							else if (toucheda > intimityProgress - 50)
								petGraphicReaction(1, 1, 4, -0.05, 0);
							else if (toucheda > intimityProgress - 100)
								petGraphicReaction(0, 1, 3, -0.1, -2);
							else if (toucheda > intimityProgress - 150)
								petGraphicReaction(0, 1, 2, 0.1, -3);
							else
								petGraphicReaction(0, 0, 1, 0.8, -15);

							if (intimityProgress > 255)
								intimityProgress = 255;
						// System.out.println(intimityProgress);
					} catch (Exception e) {
					}
				}
			}
		});
	}

	public static int PoseYjthMod = 0;

	public static int DraVarEyebrown = 1;
	public static int DraVarEyes = 1;
	public static int DraVarMouth = 5;
	public static double intimityProgress = -(-255);

	void petGraphicReaction(int eyebrown, int eyes, int mouth, double intmod, int funmod) {
		DraVarEyebrown = eyebrown;
		DraVarEyes = eyes;
		DraVarMouth = mouth;
		intimityProgress += intmod;
		Timerendo.funBar += funmod;
	}

	public static boolean dispon = true;
	public static ArrayList<Point> Espuma = new ArrayList<Point>();

	static void ChangePlace(String plaz, String Posea, String dir, String Mano, int cx, int cy) {
		lugaPlace = plaz;
		if (Posea != "")
			Pose = Posea;
		inMano = Mano;
		changeCursor(dir + "/" + Mano, cx, cy);
	}

	static void changeCursor(String dirCursor, int w, int h) {
		contentPane.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
				Toolkit.getDefaultToolkit().getImage("/home/ceibal/Escritorio/Resources/Cursor/" + dirCursor + ".png"),
				new Point(w, h), "img"));
	}

	public static BufferStrategy bs;
	Graphics2D g;

	public void start() {
		createBufferStrategy(2);
		bs = getBufferStrategy();
		g = (Graphics2D) bs.getDrawGraphics();
		DrawMeth.draw(g);
	}
}
