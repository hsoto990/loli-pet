package Main;

public class EventfromRooms {
	static int faceSitMod = 0;

	public static void saaa(int x, int y) {
		if (Main.Pose.equals("Sentada"))
			faceSitMod = 92;
		else
			faceSitMod = 0;

		// EXIT BUTTON
		if (valueInCords(x, y, 299, 9, 326, 36))
			System.exit(0);

		// INICIO
		if (Main.lugaPlace.equals("Index")) {
			if (valueInCords(x, y, 248, 305, 271, 330)) {
				Main.ChangePlace("Hallroom", "De pie", "Pet", "Mano", 12, 12);
				Timerendo.start();
			}
		} else {

			// MENU INFERIOR
			if (y > 500)
				ButtonsFromLowerMenu(x, y);

			// MENU DERECHO
			else if (x > 337)
				ButtonsFromRightMenu(x, y);

			// HABITACIONES
			if (Main.lugaPlace.equals("Kitchen"))
				ButtonsFromKitchen(x, y);
			else if (Main.lugaPlace.equals("Bathroom"))
				ButtonsFromBathroom(x, y);
			else if (Main.lugaPlace.equals("Bedroom"))
				ButtonsFromBedroom(x, y);
			else if (Main.lugaPlace.equals("Shop"))
				ButtonsFromShop(x, y);
			else if (Main.lugaPlace.equals("Closet"))
				ButtonsFromCloset(x, y);
		}
	}

	static void ButtonsFromShop(int x, int y) {
		if (y > 40 & y < 125) {
			if (x > 12 & x < 64)
				Inventario.Comprar(0);
			else if (x > 95 & x < 150)
				Inventario.Comprar(1);
			else if (x > 180 & x < 240)
				Inventario.Comprar(2);
			else if (x > 270 & x < 320)
				Inventario.Comprar(3);
		} else if (y > 158 & y < 245) {
			if (x > 25 & x < 55)
				Inventario.Comprar(4);
			else if (x > 80 & x < 133)
				Inventario.Comprar(5);
			else if (x > 155 & x < 220)
				Inventario.Comprar(6);
			else if (x > 235 & x < 330)
				Inventario.Comprar(7);
		} else if (y > 290 & y < 380) {
			if (x > 20 & x < 80)
				Inventario.Comprar(8);
			else if (x > 100 & x < 145)
				Inventario.Comprar(9);
			else if (x > 165 & x < 215)
				Inventario.Comprar(10);
			else if (x > 235 & x < 315)
				Inventario.Comprar(11);
		}
	}

	static void ButtonsFromBedroom(int x, int y) {
		// LAMPARA
		if (valueInCords(x, y, 259, 259, 259 + 74, 259 + 25)) {
			if (Main.Pose.equals("Dormida"))
				Main.Pose = "Acostada";
			else
				Main.Pose = "Dormida";
		}

	}

	static void ButtonsFromCloset(int x, int y) {
		// PEINADO
		if (valueInCords(x, y, 33, 148, 33 + 85, 148 + 25)) {
			if (DrawMeth.Hairstile == DrawMeth.HairstileMax)
				DrawMeth.Hairstile = 0;
			else
				DrawMeth.Hairstile++;
		}
		// VESTIDO
		else if (valueInCords(x, y, 218, 243, 218 + 85, 243 + 25)) {
			if (DrawMeth.DressDressed == DrawMeth.DressDressedMax)
				DrawMeth.DressDressed = 0;
			else
				DrawMeth.DressDressed++;
		}

	}

	static void ButtonsFromBathroom(int x, int y) {
		// Esponja
		if (valueInCords(x, y, 231, 180, 231 + 53, 180 + 38)) {
			if (Main.inMano.equals("Esponja")) {
				Main.changeCursor("Pet/Mano", 12, 12);
				Main.inMano = "Mano";
			} else {
				Main.changeCursor("Sponge", 26, 19);
				Main.inMano = "Esponja";
			}
		}
		if (valueInCords(x, y, 128, 55, 128 + 90, 55 + 55)) {
			int saca = Main.Espuma.size();
			for (int i = 0; i < saca; saca--)
				Main.Espuma.remove(0);
		}

	}

	static void ButtonsFromKitchen(int x, int y) {
		// BOCA
		if (valueInCords(x, y, 166, 180 - 92 + faceSitMod, 166 + 25, 180 + 15 - 92 + faceSitMod)) {
			if (Main.inMano == "Chopstick_food") {
				Main.inMano = "Chopstick";
				Main.changeCursor("Food/Chopstick", 0, 0);
				Timerendo.hungerBar += 50;
			}
		}

		// Taza de sopa
		if (valueInCords(x, y, 125, 275, 125 + 86, 275 + 53)) {

			if (Main.inMano == "Chopstick_food") {
				Main.inMano = "Chopstick";
				Main.changeCursor("Food/Chopstick", 0, 0);
			} else {
				Main.inMano = "Chopstick_food";
				Main.changeCursor("Food/Chopstick_food", 0, 0);
			}

		}
	}

	static void ButtonsFromLowerMenu(int x, int y) {
		if (x > 10 & x < 50)
			Main.ChangePlace("Shop", "", "Pet", "Mano", 12, 12);
		else if (x > 64 & x < 106) {
			Main.ChangePlace("Hallroom", "De pie", "Pet", "Mano", 12, 12);
			Main.PoseYjthMod = 0;
		} else if (x > 121 & x < 164) {
			Main.ChangePlace("Kitchen", "De pie", "Food", "Chopstick", 0, 0);
			Main.PoseYjthMod = -92;
		} else if (x > 170 & x < 220) {
			Main.ChangePlace("Bathroom", "De pie", "Pet", "Mano", 12, 12);
			DrawMeth.DressDressed = -1;
			Main.PoseYjthMod = 0;
		} else if (x > 236 & x < 278) {
			Main.ChangePlace("Bedroom", "Acostada", "Pet", "Mano", 12, 12);
			Main.PoseYjthMod = -50;
		} else if (x > 298 & x < 325) {
			Main.ChangePlace("Closet", "De pie", "Pet", "Mano", 12, 12);
			Main.PoseYjthMod = -0;
		}
	}

	static void ButtonsFromRightMenu(int x, int y) {
		// BOTONES DE VELOCIDAD
		if (y > 15 & y < 40) {
			if (x > 350 & x < 375)
				Timerendo.veltime = 1000;
			else if (x > 380 & x < 405)
				Timerendo.veltime = 100;
			else if (x > 410 & x < 435)
				Timerendo.veltime = 10;
		}

		// BOTON SENTARSE
		if (!(Main.lugaPlace.equals("Bedroom") | (Main.lugaPlace.equals("Shop")))) {
			if (valueInCords(x, y, 350, 48, 350 + 64, 48 + 25))
				if (Main.Pose.equals("De pie"))
					Main.Pose = "Sentada";
				else
					Main.Pose = "De pie";
		}

		// AUTO-COMER
		if (valueInCords(x, y, 350, 85, 350 + 64, 85 + 25)) {
			new Thread() {
				public void run() {
					AutoMoves.eat(5);
				}
			}.start();
		}

		// AUTO-MODE
		if (valueInCords(x, y, 350, 137, 350 + 64, 137 + 25))
			Main.AutoMode = !Main.AutoMode;

	}

	static boolean valueInCords(int mx, int my, int x, int y, int x2, int y2) {
		return (mx >= x & mx <= x2) & (my >= y & my <= y2);
	}

}
