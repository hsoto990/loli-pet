package Main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class DrawMeth {
	static void draw(Graphics2D g) {
		new Thread() {
			public void run() {
				while (true) {
					try {

						// FONDO
						drawimageEqSize(g, "Places/" + Main.lugaPlace, 0, 0);
						g.setColor(new Color(215, 215, 215));
						g.fillRect(337, 0, 437, 550);

						if (!Main.lugaPlace.equals("Index")) {

							// TIENDA
							if (Main.lugaPlace.equals("Shop")) {
							}
							// COCINA
							else if (Main.lugaPlace.equals("Kitchen")) {
								drawBody(g, 0, -92);
								drawimageEqSize(g, "Objects/Table", 0, 550 - 276);
								drawimageEqSize(g, "Objects/Sopa", 125, 275);
							}
							// HABITACION
							else if (Main.lugaPlace.equals("Bedroom")) {
								drawBody(g, 0, -50);
								if (Main.Pose.equals("Dormida"))
									g.setColor(new Color(184, 207, 229));
								else
									g.setColor(new Color(215, 228, 241));
								draButton(g, "Lamp", 259, 259);
							}
							// BAÑO
							else if (Main.lugaPlace.equals("Bathroom")) {
								drawBody(g, 0, 0);
								if (!Main.inMano.equals("Esponja"))
									drawimageEqSize(g, "Objects/Sponge", 231, 180);
								for (int i = 0; i < Main.Espuma.size(); i++) {
									drawimageEqSize(g, "Objects/Espuma", (int) Main.Espuma.get(i).getX(),
											(int) Main.Espuma.get(i).getY());

								}

							} // CLOSET
							else if (Main.lugaPlace.equals("Closet")) {
								drawBody(g, 0, 0);
								g.setColor(new Color(215, 228, 241));

								g.fillRect(33, 148, 85, 25);
								g.fillRect(218, 243, 85, 25);

								g.setColor(Color.black);
								drawBoldString(g, "Peinado", 33 + 22, 148 + 17);

								drawBoldString(g, "Vestido", 218 + 22, 243 + 17);
							}
							// OTROS
							else
								drawBody(g, 0, 0);

							// MENU INFERION
							drawimageEqSize(g, "GUI/StatBar", 0, 500);

							// MENU DERECHO

							// BOTON SENTARSE
							if (!(Main.lugaPlace.equals("Bedroom") | Main.lugaPlace.equals("Shop"))) {
								if (Main.Pose.equals("Sentada"))
									g.setColor(new Color(184, 207, 229));
								else
									g.setColor(new Color(215, 228, 241));
								draButton(g, "Sit", 350, 48);
							}
							// BOTON COMER
							g.setColor(new Color(215, 228, 241));
							draButton(g, "Eat", 350, 85);

							// BOTON AUTONOMIA
							if (Main.AutoMode)
								g.setColor(new Color(184, 207, 229));
							else
								g.setColor(new Color(215, 228, 241));
							draButton(g, "Auto", 350, 137);

							// BOTONES DE VELOCIDAD

							// 1Min/seg
							if (Timerendo.veltime == 1000)
								g.setColor(new Color(184, 207, 229));
							else
								g.setColor(new Color(215, 228, 241));
							g.fillRect(350, 15, 25, 25);
							if (Timerendo.veltime == 100)
								g.setColor(new Color(184, 207, 229));
							else
								g.setColor(new Color(215, 228, 241));
							g.fillRect(380, 15, 25, 25);
							if (Timerendo.veltime == 10)
								g.setColor(new Color(184, 207, 229));
							else
								g.setColor(new Color(215, 228, 241));
							g.fillRect(410, 15, 25, 25);

							g.setColor(Color.black);
							drawBoldString(g, ">", 358, 15 + 17);
							drawBoldString(g, ">>", 384, 15 + 17);
							drawBoldString(g, ">>>", 410, 15 + 17);
							// PARAMETROS PERSONALES
							drawStats(g);
							
							if (Main.intimityProgress>255-42.5*1)
								drawimageEqSize(g, "Reeel/0", 0, 100);
							else if (Main.intimityProgress>255-42.5*2)
								drawimageEqSize(g, "Reeel/1", 0, 100);
							else if (Main.intimityProgress>255-42.5*3)
								drawimageEqSize(g, "Reeel/2", 0, 100);
							else if (Main.intimityProgress>255-42.5*4)
								drawimageEqSize(g, "Reeel/3", 0, 100);
							else if (Main.intimityProgress>255-42.5*5)
								drawimageEqSize(g, "Reeel/4", 0, 100);
							else if (Main.intimityProgress>255-42.5*6)
								drawimageEqSize(g, "Reeel/5", 0, 100);
							else 
								drawimageEqSize(g, "Reeel/6", 0, 100);

						}

						// EXIT BUTTON
						drawimageEqSize(g, "GUI/ExitCross", 300, 10);

						Main.bs.show();
						// Thread.sleep(10);

					} catch (Exception e) {
					}
				}
			}
		}.start();
	}

	static void drawStats(Graphics g) {

		if (Timerendo.hungerBar < 0)
			Timerendo.hungerBar = 0;
		else if (Timerendo.hungerBar > Timerendo.hungerBarMax)
			Timerendo.hungerBar = Timerendo.hungerBarMax;
		if (Timerendo.sleepBar < 0)
			Timerendo.sleepBar = 0;
		else if (Timerendo.sleepBar > Timerendo.sleepBarMax)
			Timerendo.sleepBar = Timerendo.sleepBarMax;
		if (Timerendo.cleanBar < 0)
			Timerendo.cleanBar = 0;
		else if (Timerendo.cleanBar > Timerendo.cleanBarMax)
			Timerendo.cleanBar = Timerendo.cleanBarMax;
		if (Timerendo.funBar < 0)
			Timerendo.funBar = 0;
		else if (Timerendo.funBar > Timerendo.funBarMax)
			Timerendo.funBar = Timerendo.funBarMax;

		if (Timerendo.IMCValue <= 16)
			System.exit(0);

		String hungerLabel = "";
		String sleepLabel = "";
		String cleanLabel = "";
		String funLabel = "";
		if (Timerendo.hungerBar == 0) {
			hungerLabel = "Estomago Vacío";
			Timerendo.funBar -= 1;
		} else if (Timerendo.hungerBar <= 250)
			hungerLabel = "Satisfecha";
		else if (Timerendo.hungerBar <= 500)
			hungerLabel = ("Llena");
		else if (Timerendo.hungerBar <= 750) {
			hungerLabel = ("Rellena");
			Timerendo.funBar -= 1;
		} else {
			hungerLabel = ("A reventar");
			Timerendo.funBar -= 2;
		}

		if (Timerendo.sleepBar <= 2 * 60) {
			sleepLabel = ("Agotada");
			Timerendo.funBar -= 3;
		} else if (Timerendo.sleepBar <= 4 * 60) {
			sleepLabel = ("Muy cansada");
			Timerendo.funBar = -2;
		} else if (Timerendo.sleepBar <= 8 * 60) {
			sleepLabel = ("Cansada");
			Timerendo.funBar -= 1;
			// emoimageset(EmotionFaceSleepy, "Others/Sleepy");
		} else if (Timerendo.sleepBar <= 16 * 60) {
			sleepLabel = ("Algo cansada");
			// emoimageset(EmotionFaceSleepy, "Others/null");
		} else {
			sleepLabel = ("Despierta");
			Timerendo.funBar += 1;
		}

		if (Timerendo.cleanBar <= 2 * 60) {
			cleanLabel = ("Apesta");
			Timerendo.funBar -= 2;
		} else if (Timerendo.cleanBar <= 8 * 60) {
			cleanLabel = ("Huele muy mal");
			Timerendo.funBar -= 1;
		} else if (Timerendo.cleanBar <= 16 * 60)
			cleanLabel = ("Huele algo mal");
		else if (Timerendo.cleanBar <= 26 * 60) {
			cleanLabel = ("No huele mal");
		} else {
			cleanLabel = ("Huele bien");
			Timerendo.funBar += 1;
		}

		if (Timerendo.funBar <= 13 * 60)
			funLabel = ("Está deprimida");
		else if (Timerendo.funBar <= 26 * 60)
			funLabel = ("Está muy triste");
		else if (Timerendo.funBar <= 39 * 60)
			funLabel = ("Está triste");
		else if (Timerendo.funBar <= 52 * 60)
			funLabel = ("No está felíz");
		else
			funLabel = ("Está felíz");

		g.setColor(new Color(215, 228, 241));
		g.fillRect(0, 0, 100, 80);
		g.fillRect(195, 12, 70, 15);
		g.setColor(Color.black);
		drawBoldString(g, hungerLabel, 5, 15);
		drawBoldString(g, sleepLabel, 5, 30);
		drawBoldString(g, cleanLabel, 5, 45);
		drawBoldString(g, funLabel, 5, 60);
		drawBoldString(g, "IMC=" + ((int) (Timerendo.IMCValue * 100)) / 100.00, 5, 75);

		if (Timerendo.hora < 10) {
			if (Timerendo.minu < 10)
				drawBoldString(g, " " + Timerendo.hora + ":0" + Timerendo.minu + " Dia " + Timerendo.dia, 196, 24);
			else
				drawBoldString(g, " " + Timerendo.hora + ":" + Timerendo.minu + " Dia " + Timerendo.dia, 196, 24);
		} else {
			if (Timerendo.minu < 10)
				drawBoldString(g, Timerendo.hora + ":0" + Timerendo.minu + " Dia " + Timerendo.dia, 196, 24);
			else
				drawBoldString(g, Timerendo.hora + ":" + Timerendo.minu + " Dia " + Timerendo.dia, 196, 24);
		}

	}

	static BufferedImage BuffImageSelect(String stinga) throws Exception {
		return ImageIO.read(new File("/home/ceibal/Escritorio/Resources/" + stinga + ".png"));
	}

	static void drawBoldString(Graphics g, String stinga, int x, int y) {
		g.drawString(stinga, x, y);
		g.drawString(stinga, x + 1, y);
	}

	static void draButton(Graphics g, String text, int x, int y) {
		g.fillRect(x, y, 74, 25);
		g.setColor(Color.black);
		drawBoldString(g, text, x + 22, y + 17);
	}

	static void drawimageEqSize(Graphics g, String stinga, int x, int y) {
		try {
			BufferedImage sa = BuffImageSelect(stinga);
			g.drawImage(sa, x, y, sa.getWidth(), sa.getHeight(), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int DressDressed = 0;
	public static int DressDressedMax = 1;
	public static int Hairstile = 0;
	public static int HairstileMax = 2;

	static void drawBody(Graphics g, int x, int y) {
		drawimageEqSize(g, "Poses/" + Main.Pose, x, y);
		if (Main.Pose.equals("Sentada"))
			y += 93;

		drawimageEqSize(g, "Emotions/Eyebrown/" + Main.DraVarEyebrown, x + 137, y + 125);
		if (Main.Pose.equals("Dormida"))
			drawimageEqSize(g, "Emotions/Eyes/2", x + 137, y + 125);
		else
			drawimageEqSize(g, "Emotions/Eyes/" + Main.DraVarEyes, x + 137, y + 125);
		drawimageEqSize(g, "Emotions/Mouth/" + Main.DraVarMouth, x + 137, y + 125);
		drawimageEqSize(g, "Dresses/" + DressDressed, x, y);
		drawimageEqSize(g, "Peinados/"+Hairstile, x, y);
	}

}
